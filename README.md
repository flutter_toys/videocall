# VideoCall through  <mark style= 'background-color: #f1f8ff'> the AgoraAPI </mark>

Currently, there is the **error** the video cannot be loaded, 
Seems to be caused by a conflict between Agora API and **certain** Android emulators. → [Stackoverflow](https://stackoverflow.com/questions/65551415/failed-to-load-dynamic-library-in-flutter-app)

In my case, I've debugged with this emulator → <mark style= 'background-color: #f1f8ff'>**pixel 2 API31**</mark>, so I want you to use this emulator on this project.


## Getting Started
> 1. https://www.agora.io/en/ ← You should create your own channel (server to communicate) within the Agora site and bring the information there.
```
const APP_ID = '';

const CHANNEL_NAME = '';

const TEMP_TOKEN ='';
```

>2. the packages currently using
```
  agora_rtc_engine: ^6.0.0
  permission_handler: ^10.0.2
  sqlite3_flutter_libs: ^0.4.2
```
**CAUTION** NOT DESCRIPTIONED HERE, YOU MUST READ EACH PACKAGE GUIDE AND PROCEED FURTHER SETTINGS!  →  [here](https://pub.dev/)

## system snapshots

<details>
  <summary>Application snapshot</summary>
  <img src="https://gitlab.com/flutter_toys/videocall/uploads/f071781f61a41f599aac454623cdff3d/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-16_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_11.00.33.png" width = "50%"name="image-name">
  <img src="https://gitlab.com/flutter_toys/videocall/uploads/4a647dd9067645a31537133ab9415049/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-16_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_11.00.50.png" width = "50%" name="image-name">
<img src="https://gitlab.com/flutter_toys/videocall/uploads/0bc863db062e5a72cf0249c8e6a4d0f5/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-16_%E1%84%8B%E1%85%A9%E1%84%8C%E1%85%A5%E1%86%AB_11.04.46.png" width = "50%" name="image-name">
</details>

### ConnectionCheck

https://webdemo.agora.io/basicVideoCall/




## Need to fix

The problem is that the token is now stored as a const value and is only valid for **24 hours**. In the case of **production** model, a server must be implemented and a token must be issued and updated whenever video chat is made.

<details>
  <summary>detail</summary>
<img src="https://gitlab.com/flutter_toys/videocall/uploads/e74afc6de99c49f659a094c41d0b56bf/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-13_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_12.30.13.png" width = "100%" name="image-name">
</details>







